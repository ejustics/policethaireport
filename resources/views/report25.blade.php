@extends('layout._master')
@section('title','บันทึกข้อความ')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline">
        <label class="my-1 mr-2">ส่วนราชการ</label>
        <input type="text" class="form-control text-line col" id="service" name="service" value="">
        <label class="my-1 mr-2">โทร</label>
        <input type="text" class="form-control text-line col-sm-2" id="tell" name="tell"  value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ที่</label>
        <input type="text" class="form-control text-line col" id="no" name="no" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เรื่อง</label>
        <input type="text" class="form-control text-line col" id="subject" name="subject" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เรียน</label>
        <input type="text" class="form-control text-line col" id="to" name="to" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5">พนักงานสอบสวนขอส่งของกลางมาเพื่อให้ตรวจพิสูจน์ดังนี้</label>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๑) </label>
        <label class="my-1 mr-2">คดีที่</label>
        <input type="text" class="form-control text-line col-sm-2" id="case_no" name="case_no" value="">
        <label class="my-1 mr-2">ชื่อผู้ต้องหา</label>
        <input type="text" class="form-control text-line col" id="accused" name="accused" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2"></label>
        <label class="my-1 mr-2">อายุ</label>
        <input type="text" class="form-control text-line col-sm-1" id="age" name="age" value="">
        <label class="my-1 mr-2">ปี</label>
        <label class="my-1 mr-2">สัญชาติ</label>
        <input type="text" class="form-control text-line col-sm-1" id="nationality" name="nationality" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๒) </label>
        <label class="my-1 mr-2">วัน เดือน ปี ที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="case_date" name="case_date" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๓) </label>
        <label class="my-1 mr-2">สถานที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="case_loc" name="case_loc" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๔) </label>
        <label class="my-1 mr-2">ชื่อผู้กล่าวหา</label>
        <input type="text" class="form-control text-line col" id="accuser" name="accuser" value="">
        <label class="my-1 mr-2">อายุ</label>
        <input type="text" class="form-control text-line col-sm-1" id="age2" name="age2" value="">
        <label class="my-1 mr-2">ปี</label>
        <label class="my-1 mr-2">สัญชาติ</label>
        <input type="text" class="form-control text-line col-sm-1" id="nationality2" name="nationality2" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๕) </label>
        <label class="my-1 mr-2">ฐานความผิด</label>
        <input type="text" class="form-control text-line col" id="accusation" name="accusation" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๖) </label>
        <label class="my-1 mr-2">พฤติการณ์และข้อเท็จจริงที่เกิดขึ้น</label>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <textarea type="text" class="form-control text-line col" rows="3" id="case_detail" name="case_detail" value=""></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๗) </label>
        <label class="my-1 mr-2">สถานที่พบของกลาง</label>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <textarea type="text" class="form-control text-line col" rows="2" id="item_loc" name="item_loc" value=""></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๘) </label>
        <label class="my-1 mr-2">รายการของกลางที่ส่งมาตรวจ</label>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๘.๑)</label>
        <input type="text" class="form-control text-line col" id="item1" name="item1" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๘.๒)</label>
        <input type="text" class="form-control text-line col" id="item2" name="item2" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๘.๓)</label>
        <input type="text" class="form-control text-line col" id="item3" name="item3" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๙) </label>
        <label class="my-1 mr-2">จุดประสงค์ในการตรวจพิสูจน์</label>
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๙.๑)</label>
        <input type="text" class="form-control text-line col" id="p_item1" name="p_item1" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๙.๒)</label>
        <input type="text" class="form-control text-line col" id="p_item2" name="p_item2" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5"></label>
        <label class="my-1 mr-2">(๙.๓)</label>
        <input type="text" class="form-control text-line col" id="p_item3" name="p_item3" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 ml-5 mr-2">(๑o) </label>
        <label class="my-1 mr-2">ชื่อพนักงานสอบสวน</label>
        <input type="text" class="form-control text-line col" id="inquiry_officer" name="inquiry_officer" value="">
    </div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">ลงชื่อ</label>
        <input type="text" class="form-control text-line col-sm-4" id="staff_name" name="staff_name" value="">
    </div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">ตำแหน่ง</label>
        <input type="text" class="form-control text-line col-sm-4" id="staff_position" name="staff_position" value="">
    </div>
</form>
@endsection
@section('script')
<script>
</script>
@endsection
