@extends('layout._master')
@section('title','แผนที่สังเขปแสดงสถานที่เกิดเหตุ')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="row">
        <div class="col-sm-2 pt-4">คดีระหว่าง</div>
        <div class="col-sm-10">
            <div class="form-inline">
                <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accuser" name="accuser" value="">
                <label class="my-1 mr-2">ผู้กล่าวหา</label>
            </div>
            <div class="form-inline">
                <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accused" name="accused" value="">
                <label class="my-1 mr-2">ผู้ต้องหา</label>
            </div>
        </div>
    </div>
    <div class="form-inline">
            <label class="my-1 mr-2">ฐานความผิด</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accusation" name="accusation" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ที่เกิดเหตุ</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="str" name="str" value="">
        <label class="my-1 mr-2">หมู่ที่</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col-sm-1" id="n" name="n" value="">
        <label class="my-1 mr-2">ตรอก/ซอย</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="soi" name="soi" value="">
        <label class="my-1 mr-2">ถนน</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="road" name="road" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">แขวง/ตำบล</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="district" name="district" value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="city" name="city" value="">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="province" name="province" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เมื่อวันที่</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="day" name="day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="month" name="month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="year" name="year" value="">
        <label class="my-1 mr-2">เวลาประมาณ</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="time" name="time" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">คำอธิบายแผนที่</label>
    </div>
    <div class="form-inline">
        <textarea class="form-control text-line col" id="desc" name="desc" rows="3"></textarea>
    </div>

</form>
@endsection
@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<script>
    function myMap() {
        var mapOptions = {
            center: new google.maps.LatLng(51.5, -0.12),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.HYBRID
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }
</script>
@endsection
