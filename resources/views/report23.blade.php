@extends('layout._master')
@section('title','รายงานการชันสูตรพลิกศพ')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline">
        <label class="my-1 mr-2">ที่</label>
        <input type="text" class="form-control text-line col-sm-1" id="no" name="no" value="">
        <label class="my-1 mr-2">/๒๕ สถานีตำรวจ</label>
        <input type="text" class="form-control text-line col" id="station" name="station"  value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control text-line col" id="s_city" name="s_city"  value="">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control text-line col" id="s_province" name="s_province"  value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-2" id="day" name="day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col" id="month" name="month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col" id="year" name="year" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อผู้ตาย</label>
        <input type="text" class="form-control text-line col" id="dead_name" name="dead_name" value="">
        <label class="my-1 mr-2">อายุ</label>
        <input type="text" class="form-control text-line col-sm-1" id="dead_age" name="dead_age" value="">
        <label class="my-1 mr-2">ปี</label>
        <label class="my-1 mr-2">เพศ</label>
        <input type="text" class="form-control text-line col-sm-2" id="dead_sex" name="dead_sex" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เชื้อชาติ</label>
        <input type="text" class="form-control text-line col" id="dead_race" name="dead_race" value="">
        <label class="my-1 mr-2">สัญชาติ</label>
        <input type="text" class="form-control text-line col" id="dead_nationality" name="dead_nationality" value="">
        <label class="my-1 mr-2">ศาสนา</label>
        <input type="text" class="form-control text-line col" id="dead_religion" name="dead_religion" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อบิดา</label>
        <input type="text" class="form-control text-line col" id="dead_father" name="dead_father" value="">
        <label class="my-1 mr-2">ชื่อมารดา</label>
        <input type="text" class="form-control text-line col" id="dead_mather" name="dead_mather" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ที่อยู่ บ้านเลขที่</label>
        <input type="text" class="form-control text-line col-sm-2" id="address" name="address" value="">
        <label class="my-1 mr-2">หมู่</label>
        <input type="text" class="form-control text-line col-sm-1" id="n" name="n" value="">
        <label class="my-1 mr-2">แขวง/ตำบล</label>
        <input type="text" class="form-control text-line col" id="ditrict" name="ditrict" value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control text-line col" id="city" name="city" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control text-line col-sm-2" id="province" name="province" value="">
        <label class="my-1 mr-2">วัน เวลาที่ตาย</label>
        <input type="text" class="form-control text-line col" id="dead_date" name="dead_date" value="">
        <label class="my-1 mr-2">วัน เวลาที่พบศพ</label>
        <input type="text" class="form-control text-line col" id="find_date" name="find_date" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ที่ที่ตาย</label>
        <input type="text" class="form-control text-line col" id="d_address" name="d_address" value="">
        <label class="my-1 mr-2">แขวง/ตำบล</label>
        <input type="text" class="form-control text-line col" id="d_ditrict" name="d_ditrict" value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control text-line col" id="d_city" name="d_city" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control text-line col-sm-2" id="d_province" name="d_province" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ที่ที่พบศพ</label>
        <input type="text" class="form-control text-line col" id="f_address" name="f_address" value="">
        <label class="my-1 mr-2">แขวง/ตำบล</label>
        <input type="text" class="form-control text-line col" id="f_ditrict" name="f_ditrict" value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control text-line col" id="f_city" name="f_city" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control text-line col-sm-2" id="f_province" name="f_province" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อผู้ที่ทำให้ตาย ถ้ามี</label>
        <input type="text" class="form-control text-line col" id="assassin" name="assassin" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อผู้พบศพ</label>
        <input type="text" class="form-control text-line col" id="founder" name="founder" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อและตำแหน่งพนักงานผู้ทำการชันสูตรพลิกศพ</label>
        <input type="text" class="form-control text-line col" id="staff_dead" name="staff_dead" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เหตุและพฤติการณ์ที่ตาย</label>
        <textarea type="text" class="form-control text-line col" id="root_cause" name="root_cause" rows="2" value=""></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ได้จัดการแก่ศพนั้นอย่างไร</label>
        <input type="text" class="form-control text-line col" id="deal_body" name="deal_body" value="">
    </div>
    <hr>
    <div class="mx-auto text-center" style="width: 40%;">
        <h3>ความเห็นของแพทย์หรือพนักงานแยกธาตุ</h3>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">๑. สภาพของศพ หรือส่วนของศพตามที่พบเห็นหรือตามที่ปรากฏจากการตรวจพร้อมความเห็น</label>
    </div>
    <div class="form-inline">
        <textarea type="text" class="form-control text-line col" id="remark" name="remark" rows="5" value=""></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">๒. แสดงเหตุที่ตายเท่าทีทำได้</label>
    </div>
    <div class="form-inline">
        <textarea type="text" class="form-control text-line col" id="s_root_cause" name="s_root_cause" rows="5" value=""></textarea>
    </div>
    <div class="clearfix"></div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="rm_day" name="rm_day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col-sm-2" id="rm_month" name="rm_month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col-sm-" id="rm_year" name="rm_year" value="">
    </div>
</form>
@endsection
@section('script')
<script>
</script>
@endsection
