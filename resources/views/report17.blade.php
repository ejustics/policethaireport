@extends('layout._master')
@section('title','บันทึกการตรวจสถานที่เกิดเหตุคดีอาญา')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline float-right">
        <label class="my-1 mr-2">สถานที่บันทึก</label>
        <input type="text" class="form-control text-line col" id="station" name="station" value="">
    </div>
    <div class="clearfix"></div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="day" name="day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col-sm-1" id="month" name="month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col-sm-1" id="year" name="year" value="">
    </div>
    <div class="clearfix"></div>
    <p class="text-center">เจ้ัาพนักงานได้ตรวจสถานที่เกิดเหตุในคดีนี้ ปรากฎรายละเอียด ดังนี้</p>
    <div class="clearfix"></div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 1</label>
        <label class="my-1 mr-2">ผู้ตรวจสถานที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="site_inspector" name="site_inspector" value="">
        <label class="my-1 mr-2">ตำแหน่ง</label>
        <input type="text" class="form-control text-line col" id="inspector_pos" name="inspector_pos" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 2</label>
        <label class="my-1 mr-2">ผู้กล่าวหา</label>
        <input type="text" class="form-control text-line col" id="accuser" name="accuser" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 3</label>
        <label class="my-1 mr-2">ผู้ต้องหา</label>
        <input type="text" class="form-control text-line col" id="accused" name="accused" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 4</label>
        <label class="my-1 mr-2">ฐานความผิด</label>
        <textarea class="form-control text-line col" rows="1" id="accusation" name="accusation" value=""></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 5</label>
        <label class="my-1 mr-2">วันเวลาที่เกิดเหตุ</label>
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="d_acd" name="d_acd" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col" id="m_acd" name="m_acd" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col" id="y_acd" name="y_acd" value="">
        <label class="my-1 mr-2">เวลา</label>
        <input type="text" class="form-control text-line col" id="t_acd" name="t_acd" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 6</label>
        <label class="my-1 mr-2">วันเวลารับแจ้งเหตุ</label>
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="d_noti" name="d_noti" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col" id="m_noti" name="m_noti" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col" id="y_noti" name="y_noti" value="">
        <label class="my-1 mr-2">เวลา</label>
        <input type="text" class="form-control text-line col" id="t_noti" name="t_noti" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">ป.จ.ว ข้อ</label>
        <input type="text" class="form-control text-line col" id="str_noti" name="str_noti" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 7</label>
        <label class="my-1 mr-2">วัน เวลาตรวจสถานที่เกิดเหตุ</label>
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="d_loc" name="d_loc" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col" id="m_loc" name="m_loc" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col" id="y_loc" name="y_loc" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">เวลาระหว่าง</label>
        <input type="text" class="form-control text-line col-sm-2" id="s_t_loc" name="s_t_loc" value="">
        <label class="my-1 mr-2">น.</label>
        <label class="my-1 mr-2">ถึง</label>
        <input type="text" class="form-control text-line col-sm-2" id="e_t_loc" name="e_t_loc" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 8</label>
        <label class="my-1 mr-2">สถานที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="str_loc" name="str_loc" value="">
        <label class="my-1 mr-2">หมู่ที่</label>
        <input type="text" class="form-control text-line col" id="n_loc" name="n_loc" value="">
        <label class="my-1 mr-2">ตรอก/ซอย</label>
        <input type="text" class="form-control text-line col" id="soi_loc" name="soi_loc" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">ถนน</label>
        <input type="text" class="form-control text-line col" id="road_loc" name="road_loc" value="">
        <label class="my-1 mr-2">แขวง/ตำบล</label>
        <input type="text" class="form-control text-line col" id="district_loc" name="district_loc" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control text-line col" id="city_loc" name="city_loc" value="">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control text-line col" id="province_loc" name="province_loc" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 9</label>
        <label class="my-1 mr-2">สภาพทั่วไป และร่องรอยหลักฐานที่พบในสถานที่เกิดเหตุ</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">9.1</label>
        <textarea class="form-control text-line col" id="scene_1" name="scene_1"  rows="3"></textarea>
    </div>
    <div class="clearfix"></div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <label class="my-1 mr-2">9.2</label>
        <textarea class="form-control text-line col" id="scene_2" name="scene_2" rows="3"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 10</label>
        <label class="my-1 mr-2">เหตุการณ์ที่เกิดขึ้นโดยสังเขป</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <textarea class="form-control text-line col" id="event" name="event" rows="10"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 11</label>
        <label class="my-1 mr-2">ความเสียหายต่อชีวิต ร่างกายหรือทรัพย์สิน ที่เกิดขึ้น</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <textarea class="form-control text-line col" id="damage" name="damage" rows="5"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 12</label>
        <label class="my-1 mr-2">สิ่งของที่ยึดได้จากที่เกิดเหตุ (อธิบายให้ชัดเจนว่า สิ่งของชิ้นใด ยึดมากจาที่ใด แยกเป็นข้อย่อยแต่ละชิ้อเพื่อมิให้สับสน)</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <textarea class="form-control text-line col" id="item_desc" name="item_desc" rows="5"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ข้อ 13</label>
        <label class="my-1 mr-2">ข้อสันนิษฐานเบื้องต้น</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2"></label>
        <textarea class="form-control text-line col" id="assumptions" name="assumptions"  rows="5"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ลงชื้อ</label>
        <input type="text" class="form-control text-line col-sm-4" id="sign_name" name="sign_name" value="">
        <label class="my-1 mr-2 col-sm-2">ผู้ตรวจบันทึก</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2 col-sm-2">ตำแหน่ง</label>
        <input type="text" class="form-control text-line  col-sm-4" id="sign_pos" name="sign_pos" value="">
        <label class="my-1 mr-2 col-sm-2"></label>
    </div>
</form>
@endsection
@section('script')
<script>
</script>
@endsection
