@extends('layout._master')
@section('title','บันทึกพนักงานสอบสวน')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline">
        <label class="my-1 mr-2">สถานีตำรวจ/หน่วยงาน</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="station" name="station" value="">
        <label class="my-1 mr-2">เขต/อำเภอ</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="city" name="city" value="">
        <label class="my-1 mr-2">จังหวัด</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="province" name="province" value="">
    </div>
    <div class="form-inline">
        <button type="button" class="btn btn-info ml-4 my-1" onclick="addRow();">+ เพิ่ม</button>
    </div>
    <table id="table-list" class="table table-bordered border-0">
        <tr class="border-left-0">
            <th class="border-0"></th>
            <th class="text-center">วัน เดือน ปี</th>
            <th class="text-center" style="width: 40%;">รายการปฎิบัติ</th>
            <th class="text-center">ลายมือชื่อ และ <br>ตำแหน่งผู้บันทึก</th>
        </tr>
        <tr>
            <td class="td-input border-0">
                <button type="button" class="btn btn-danger btn-block btn-remove-row" onclick="deleteRow();"><i class="fas fa-times mt-1"></i></button>
            </td>
            <td class="td-input">
                <input type="text" name="date[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="text" name="desc[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="text" name="signature[]" class="form-control text-center">
            </td>
        </tr>
    </table>
</form>
@endsection
@section('script')
<script>
    function addRow() {

        var sRow =
            `<tr>
            <td class="td-input border-0">
                <button type="button" class="btn btn-danger btn-block btn-remove-row" onclick="deleteRow();"><i class="fas fa-times mt-1"></i></button>
            </td>
            <td class="td-input">
                <input type="text" name="date[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="text" name="desc[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="text" name="signature[]" class="form-control text-center">
            </td>
        </tr>`;
        $('#table-list tr:last').after(sRow);
    }

    function deleteRow(control) {

        if (confirm('แน่นใจว่าต้องการ ลบแถวข้อมูลนี้')) {
            $(control).closest('tr').remove();
        }
    }
</script>
@endsection
