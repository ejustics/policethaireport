<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <title>@yield('title')</title>
    <style>
        @font-face {
            font-family: THSarabunNew;
            src: url('/fonts/THSarabunNew.ttf');
            font-weight: normal;
            font-style: normal;
        }

        html,
        body {
            font-family: 'THSarabunNew';
            font-size: 1.2rem;
            background-color: #eee;
            height: 100%;
            min-height: 100%;
        }

        .text-line {
            background-color: transparent;
            color: #000;
            outline: none;
            outline-style: none;
            outline-offset: 0;
            border-top: none;
            border-left: none;
            border-right: none;
            border-bottom: solid #000 1px;
            border-bottom-style: dotted;
            border-radius: 0;
            padding: 3px 10px;
            font-size: 16pt;
        }

        .td-input {
            padding: 0 !important;
        }

        .td-input > input, textarea {
            padding: 0 .25rem !important;
            border-radius: 0;
        }
        .td-input > .btn-remove-row {
            padding: 0 .25rem;
            height: 30px;
            opacity: 0.6;
        }
        .td-input > .btn-remove-row:hover {
            opacity: 1;
        }
    </style>
    @yield('style')
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2" style="background-color: #fff; height: 100%;">
                <nav class="navbar sticky-top navbar-dark bg-primary">
                    <a class="navbar-brand" href="#">รายการ สำนวน</a>
                </nav>
                <hr />
                <div>
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{$id == 11 ? 'active' : ''}}" href="/report/11">
                                <i class="fas fa-file"></i>
                                บัญชีของกลาง
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 12 ? 'active' : ''}}" href="/report/12">
                                <i class="fas fa-file"></i>
                                บัญชีทรัพย์ถูกประทุษร้าย </a> </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 13 ? 'active' : ''}}" href="/report/13">
                                <i class="fas fa-file"></i>
                                บัญชีทรัพย์ถูกประทุษร้ายได้คืน
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 14 ? 'active' : ''}}" href="/report/14">
                                <i class="fas fa-file"></i>
                                บัญชีทรัพย์ถูกประทุษร้ายไม่ได้คืน
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 15 ? 'active' : ''}}" href="/report/15">
                                <i class="fas fa-file"></i>
                                บัญชีทรัพย์ถูกเพลิงไหม้
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 16 ? 'active' : ''}}" href="/report/16">
                                <i class="fas fa-file"></i>
                                บันทึกพนักงานสอบสวน
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 17 ? 'active' : ''}}" href="/report/17">
                                <i class="fas fa-file"></i>
                                บันทึกการตรวจสถานที่เกิดเหตุคดีอาญา
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 19 ? 'active' : ''}}" href="/report/19">
                                <i class="fas fa-file"></i>
                                แผนที่สังเขปแสดงสถานที่เกิดเหตุ
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 20 ? 'active' : ''}}" href="/report/20">
                                <i class="fas fa-file"></i>
                                บันทึกการนำชี้ที่เกิดเหตุประกอบคำรับสารภาพ
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 21 ? 'active' : ''}}" href="/report/21">
                                <i class="fas fa-file"></i>
                                ใบนำส่งผู้บาดเจ็บหรือศพให้แพทย์ตรวจชันสูตร
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 23 ? 'active' : ''}}" href="/report/23">
                                <i class="fas fa-file"></i>
                                รายงานการชันสูตรพลิกศพ
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$id == 25 ? 'active' : ''}}" href="/report/25">
                                <i class="fas fa-file"></i>
                                บันทึกส่งของกลางมาตรวจพิสูจน์
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-10">
                <nav class="navbar sticky-top navbar-dark bg-dark">
                    <a class="navbar-brand" href="#"><b>สำนวน</b> : @yield('title')</a>
                    <div class="form-inline">
                        <button class="btn btn-warning mr-sm-2" onclick="document.getElementById('formReport').reset();"><i class="fas fa-trash"></i> ล้างข้อมูล</button>
                        <button class="btn btn-info" onclick="$('#formReport').submit();"><i class="fas fa-file-export"></i>
                            Microsoft Word</button>
                    </div>
                </nav>
                <div class="container" style="background-color: #fff; padding: 20px; height: 100%;">
                    <div class="mx-auto text-center" style="width: 40%;">
                        <h3>@yield('title')</h3>
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
    @yield('script')
</body>

</html>
