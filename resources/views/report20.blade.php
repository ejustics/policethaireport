@extends('layout._master')
@section('title','บันทึกการนำชี้ที่เกิดเหตุประกอบคำรับสารภาพ')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline float-right">
        <label class="my-1 mr-2">สถานที่บันทึก</label>
        <input type="text" class="form-control text-line col" id="station" name="station" value="">
    </div>
    <div class="clearfix"></div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="day" name="day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col-sm-1" id="month" name="month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col-sm-1" id="year" name="year" value="">
    </div>
    <div class="clearfix"></div>
    <div class="form-inline justify-content-md-center">
        <label class="my-1 mr-2">วันนี้ เวลา</label>
        <input type="text" class="form-control text-line col-ms-2" id="time" name="time" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">นามพนักงานสอบสวนที่จัดให้ผู้ต้องหานำชี้ที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="inquiry_official" name="inquiry_official" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ตำแหน่ง</label>
        <input type="text" class="form-control text-line col" id="inquiry_official_position" name="inquiry_official_position" value="">
        <label class="my-1 mr-2">พร้อมด้วย</label>
        <input type="text" class="form-control text-line col" id="with" name="with" value="">
    </div>
    <hr>
    <div class="form-inline">
        <label class="my-1 mr-2">ได้จัดให้</label>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="acc_title" id="acc_title" value="นาย">
            <label class="form-check-label" for="inlineRadio1">นาย</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="acc_title" id="acc_title" value="นาง">
            <label class="form-check-label" for="inlineRadio2">นาง</label>
        </div>
        <input type="text" class="form-control text-line col" name="accused" id="accused" value="">
    </div>
    <hr>
    <div class="form-inline">
        <label class="my-1 mr-2">ผู้ต้องหาคดีเรื่อง</label>
        <input type="text" class="form-control text-line col" name="case_name" id="case_name" value="">
    </div>
    <hr>
    <div class="form-inline">
        <label class="my-1 mr-2">วัน เดือน ปี ที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" name="case_date" id="case_date" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ซึ่งให้การรับสารภาพ นำไปชี้ที่้เกิดเหตุประกอบคำรับสารภาพ</label>
        <input type="text" class="form-control text-line col" name="confess" id="confess" value="">
    </div>
    <hr>
    <div class="form-inline">
        <label class="my-1 mr-2">ปรากฏผลกานำไปชี้สถานที่้เกิดเหตุของผู้ต้องหา ตามลำดับ ดังนี้ คือ </label>
    </div>
    <div class="form-inline">
        <textarea class="form-control text-line col" name="desc" id="desc" rows="5"></textarea>
    </div>
</form>
@endsection
@section('script')
<script>
</script>
@endsection
