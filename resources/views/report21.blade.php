@extends('layout._master')
@section('title','ใบนำส่งผู้บาดเจ็บหรือศพให้แพทย์ตรวจชันสูตร')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline">
        <label class="my-1 mr-2">คดีที่</label>
        <input type="text" class="form-control text-line col" id="case_no" name="case_no" value="">
        <label class="my-1 mr-2">สถานีตำรวจ/หน่วยงาน</label>
        <input type="text" class="form-control text-line col" id="station" name="station"  value="">
    </div>
    <div class="clearfix"></div>
    <div class="form-inline d-flex justify-content-end">
        <label class="my-1 mr-2">วัน</label>
        <input type="text" class="form-control text-line col-sm-1" id="day" name="day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col-sm-1" id="month" name="month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col-sm-1" id="year" name="year" value="">
    </div>
    <div class="clearfix"></div>
    <div class="form-inline">
        <label class="my-1 mr-2">ส่งที่</label>
        <input type="text" class="form-control text-line col" id="send_to" name="send_to" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อผู้บาดเจ็บหรือศพ</label>
        <input type="text" class="form-control text-line col" id="casualty_name" name="casualty_name" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">วันเวลาที่เกิดเหตุ</label>
        <input type="text" class="form-control text-line col" id="case_date" name="case_date" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">วันเวลาส่งผู้บาดเจ็บหรือศพ</label>
        <input type="text" class="form-control text-line col" id="casualty_send_date" name="casualty_send_date" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อผู้นำส่ง</label>
        <input type="text" class="form-control text-line col" id="sender" name="sender" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เหตุที่ต้องบาดเจ็บหรือตายด้วยประการใดโดยย่อ</label>
        <input type="text" class="form-control text-line col" id="root_cause" name="root_cause" value="">
    </div>
    <div class="form-inline">
        <textarea class="form-control text-line col" rows="9"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ลงชื่อ</label>
        <input type="text" class="form-control text-line col-sm-4" id="staff_name" name="staff_name" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ตำแหน่ง</label>
        <input type="text" class="form-control text-line col-sm-4" id="staff_position" name="staff_position" value="">
    </div>
    <br>
    <br>
    <div class="form-inline justify-content-md-center">
        <h4 class="justify-content-md-center">ผลการตรวจชั้นสูตรบาดแผลหรือศพของแพทย์</h4>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">เลขที่</label>
        <input type="text" class="form-control text-line col-sm-4" id="no" name="no" value="">
        <label class="my-1 mr-2">ตำบลที่แพทย์ตรวจ</label>
        <input type="text" class="form-control text-line col" id="district_doctor" name="district_doctor" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ชื่อของผู้บาดเจ็บหรือศพ</label>
        <input type="text" class="form-control text-line col" id="casualty_name2" name="casualty_name2" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">สถานตำรวจที่นำส่ง</label>
        <input type="text" class="form-control text-line col" id="destination" name="destination" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">รับไว้วันที่</label>
        <input type="text" class="form-control text-line col-sm-1" id="r_day" name="r_day" value="">
        <label class="my-1 mr-2">เดือน</label>
        <input type="text" class="form-control text-line col" id="r_month" name="r_month" value="">
        <label class="my-1 mr-2">พ.ศ.</label>
        <input type="text" class="form-control text-line col" id="r_year" name="r_year" value="">
        <label class="my-1 mr-2">เวลา</label>
        <input type="text" class="form-control text-line col" id="r_time" name="r_time" value="">
        <label class="my-1 mr-2">น.</label>
    </div>
    <div class="form-inline justify-content-md-center">
        <label class="my-1 mr-2">รายการที่แพทย์ตรวจและความเห็น</label>
    </div>
    <div class="form-inline">
        <textarea class="form-control text-line col" id="remark_doctor" name="remark_doctor" rows="19"></textarea>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ลงชื่อ</label>
        <input type="text" class="form-control text-line col-sm-4" id="doctor_name" name="doctor_name" value="">
        <label class="my-1 mr-2">นายแพทย์ผู้ตรวจ</label>
    </div>
</form>
@endsection
@section('script')
<script>
</script>
@endsection
