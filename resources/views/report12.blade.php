@extends('layout._master')
@section('title','บัญชีทรัพย์ถูกประทุษร้าย')
@section('content')
<form id="formReport" method="post" action="{{url('export')}}">
    {{csrf_field()}}
    <input type="hidden" name="reportId" value="{{$id}}">
    <div class="form-inline">
        <label class="my-1 mr-2">สถานีตำรวจ/หน่วยงาน</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="station" name="station" value="">
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">คดีที่่</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col-sm-2" id="cNo1" name="cNo1" value="">
        <label class="my-1 mr-2">/</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col-sm-2" id="cNo2" name="cNo2" value="">
        <label class="my-1 mr-2">ลงวันที่</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="regis_date" name="regis_date" value="">
    </div>
    <div class="row">
        <div class="col-sm-2 pt-4">คดีระหว่าง</div>
        <div class="col-sm-10">
            <div class="form-inline">
                <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accuser" name="accuser" value="">
                <label class="my-1 mr-2">ผู้กล่าวหา</label>
            </div>
            <div class="form-inline">
                <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accused" name="accused" value="">
                <label class="my-1 mr-2">ผู้ต้องหา</label>
            </div>
        </div>
    </div>
    <div class="form-inline">
        <label class="my-1 mr-2">ข้อหา/ฐานความผิด</label>
        <input type="text" class="form-control mb-2 mr-sm-2 text-line col" id="accusation" name="accusation" value="">
    </div>
    <div class="form-inline">
        <button type="button" class="btn btn-info ml-4 my-1" onclick="addRow();">+ เพิ่ม</button>
    </div>
    <table id="table-list" class="table table-bordered border-0">
        <tr class="border-left-0">
            <th class="border-0"></th>
            <th class="text-center align-middle" style="width: 8%;">ลำดับที่</th>
            <th class="text-center align-middle" style="width: 40%;">รายการ</th>
            <th class="text-center align-middle">จำนวน</th>
            <th class="text-center">ราคา<br>บาท</th>
            <th class="text-center align-middle">ของใคร</th>
            <th class="text-center">วัน เดือน ปี<br>ที่ถูกประทุษร้าย</th>
            <th class="text-center align-middle">หมายเหตุ</th>
        </tr>
        <tr>
            <td class="td-input border-0">
                <button type="button" class="btn btn-danger btn-block btn-remove-row" onclick="deleteRow();"><i class="fas fa-times mt-1"></i></button>
            </td>
            <td class="td-input">
                <input type="text" name="no[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <textarea type="text" name="desc[]" rows="1" class="form-control"></textarea>
            </td>
            <td class="td-input">
                <input type="number" name="n[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="number" name="peicr[]" class="form-control text-right">
            </td>
            <td class="td-input">
                <input type="text" name="from[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <input type="text" name="date[]" class="form-control text-center">
            </td>
            <td class="td-input">
                <textarea name="remark[]" rows="1" class="form-control"></textarea>
            </td>
        </tr>
    </table>
</form>
@endsection
@section('script')
<script>
        function addRow(){

            var sRow = `<tr>
                <td class="td-input border-0">
                    <button type="button" class="btn btn-danger btn-block btn-remove-row" onclick="deleteRow(this);"><i class="fas fa-times mt-1"></i></button>
                </td>
                <td class="td-input">
                    <input type="text" name="no[]" class="form-control text-center">
                </td>
                <td class="td-input">
                    <textarea type="text" name="desc[]" rows="1" class="form-control"></textarea>
                </td>
                <td class="td-input">
                    <input type="number" name="n[]" class="form-control text-center">
                </td>
                <td class="td-input">
                    <input type="number" name="peicr[]" class="form-control text-right">
                </td>
                <td class="td-input">
                    <input type="text" name="from[]" class="form-control text-center">
                </td>
                <td class="td-input">
                    <input type="text" id="date[]" name="date[]" class="form-control text-center">
                </td>
                <td class="td-input">
                    <textarea id="remark[]" name="remark[]" rows="1" class="form-control"></textarea>
                </td>
            </tr>`;
            $('#table-list tr:last').after(sRow);
        }
        function deleteRow(control){

            if(confirm('แน่นใจว่าต้องการ ลบแถวข้อมูลนี้')){
                $(control).closest('tr').remove();
            }
        }
    </script>
@endsection
