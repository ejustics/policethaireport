<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Report20 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report20.docx";
        $file_name = "บันทึกการนำชี้ที่เกิดเหตุประกอบคำรับสารภาพ.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('station', $request->get('station'));
        $document->setValue('day', $request->get('day'));
        $document->setValue('month', $request->get('month'));
        $document->setValue('year', $request->get('year'));
        $document->setValue('time', $request->get('time'));

        $document->setValue('time', $request->get('time'));
        $document->setValue('inquiry_official', $request->get('inquiry_official'));
        $document->setValue('inquiry_official_position', $request->get('inquiry_official_position'));
        $document->setValue('with', $request->get('with'));

        $document->setValue('acc_title', $request->get('acc_title'));
        $document->setValue('accused', $request->get('accused'));
        $document->setValue('case_name', $request->get('case_name'));
        $document->setValue('case_date', $request->get('case_date'));
        $document->setValue('confess', $request->get('confess'));
        $document->setValue('desc', $request->get('desc'));

        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
