<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Report16 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report16.docx";
        $file_name = "บันทึกพนักงานสอบสวน.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('station', $request->get('station'));
        $document->setValue('city', $request->get('city'));
        $document->setValue('province', $request->get('province'));

        //loop row
        $count = count($request->get('date'));
        $row = 24;
        if($count < $row)
            $document->cloneRow('date', $row );
        else
        {
            $document->cloneRow('date', $count);
            $row = $count;
        }

        $date = $request->input('date');
        $desc = $request->input('desc');
        $signature = $request->input('signature');

        for ($i = 0; $i < $row; $i++){
            $idx = ($i+1);
            if($i < $count){
                $document->setValue('date#'.$idx, $date[$i]);
                $document->setValue('desc#'.$idx, $desc[$i]);
                $document->setValue('signature#'.$idx, $signature[$i]);
            }else{
                $document->setValue('date#'.$idx, '');
                $document->setValue('desc#'.$idx, '');
                $document->setValue('signature#'.$idx, '');
            }
        }

        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
