<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Report19 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report19.docx";
        $file_name = "แผนที่สังเขปแสดงสถานที่เกิดเหตุ.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('accuser', $request->get('accuser'));
        $document->setValue('accused', $request->get('accused'));
        $document->setValue('accusation', $request->get('accusation'));

        $document->setValue('str', $request->get('str'));
        $document->setValue('n', $request->get('n'));
        $document->setValue('soi', $request->get('soi'));
        $document->setValue('road', $request->get('road'));
        $document->setValue('district', $request->get('district'));
        $document->setValue('city', $request->get('city'));
        $document->setValue('province', $request->get('province'));

        $document->setValue('day', $request->get('day'));
        $document->setValue('month', $request->get('month'));
        $document->setValue('year', $request->get('year'));
        $document->setValue('time', $request->get('time'));

        $document->setValue('desc', $request->get('desc'));

        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
