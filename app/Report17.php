<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Report17 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report17.docx";
        $file_name = "บันทึกการตรวจสถานที่เกิดเหตุคดีอาญา.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('station', $request->get('station'));
        $document->setValue('day', $request->get('day'));
        $document->setValue('month', $request->get('month'));
        $document->setValue('year', $request->get('year'));
        $document->setValue('site_inspector', $request->get('site_inspector'));
        $document->setValue('inspector_pos', $request->get('inspector_pos'));
        $document->setValue('accuser', $request->get('accuser'));
        $document->setValue('accused', $request->get('accused'));
        $document->setValue('accusation', $request->get('accusation'));

        $document->setValue('d_acd', $request->get('d_acd'));
        $document->setValue('m_acd', $request->get('m_acd'));
        $document->setValue('y_acd', $request->get('y_acd'));
        $document->setValue('t_acd', $request->get('t_acd'));

        $document->setValue('d_noti', $request->get('d_noti'));
        $document->setValue('m_noti', $request->get('m_noti'));
        $document->setValue('y_noti', $request->get('y_noti'));
        $document->setValue('t_noti', $request->get('t_noti'));
        $document->setValue('str_noti', $request->get('str_noti'));

        $document->setValue('d_loc', $request->get('d_loc'));
        $document->setValue('m_loc', $request->get('m_loc'));
        $document->setValue('y_loc', $request->get('y_loc'));
        $document->setValue('s_t_loc', $request->get('s_t_loc'));
        $document->setValue('e_t_loc', $request->get('e_t_loc'));
        $document->setValue('str_loc', $request->get('str_loc'));
        $document->setValue('n_loc', $request->get('n_loc'));
        $document->setValue('soi_loc', $request->get('soi_loc'));
        $document->setValue('road_loc', $request->get('road_loc'));
        $document->setValue('district_loc', $request->get('district_loc'));
        $document->setValue('city_loc', $request->get('city_loc'));
        $document->setValue('province_loc', $request->get('province_loc'));

        $document->setValue('scene_1', $request->get('scene_1'));
        $document->setValue('scene_2', $request->get('scene_2'));

        $event1 = '';
        $event2 = '';
        $document->setValue('event_1', $event1);
        $document->setValue('event_2', $event2);

        $document->setValue('damage', $request->get('damage'));
        $document->setValue('item_desc', $request->get('item_desc'));
        $document->setValue('assumptions', $request->get('assumptions'));
        $document->setValue('sign_name', $request->get('sign_name'));
        $document->setValue('sign_pos', $request->get('sign_pos'));

        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
