<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class report21 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report21.docx";
        $file_name = "ใบนำส่งผู้บาดเจ็บหรือศพให้แพทย์ตรวจชันสูตร.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('case_no', $request->get('case_no'));
        $document->setValue('station', $request->get('station'));
        $document->setValue('day', $request->get('day'));
        $document->setValue('month', $request->get('month'));
        $document->setValue('year', $request->get('year'));
        $document->setValue('send_to', $request->get('send_to'));

        $document->setValue('casualty_name', $request->get('casualty_name'));
        $document->setValue('case_date', $request->get('case_date'));
        $document->setValue('casualty_send_date', $request->get('casualty_send_date'));
        $document->setValue('sender', $request->get('sender'));
        $document->setValue('root_cause', $request->get('root_cause'));
        $document->setValue('staff_name', $request->get('staff_name'));
        $document->setValue('staff_position', $request->get('staff_position'));

        $document->setValue('no', $request->get('no'));
        $document->setValue('district_doctor', $request->get('district_doctor'));
        $document->setValue('casualty_name2', $request->get('casualty_name2'));
        $document->setValue('destination', $request->get('destination'));
        $document->setValue('r_day', $request->get('r_day'));
        $document->setValue('r_month', $request->get('r_month'));
        $document->setValue('r_year', $request->get('r_year'));
        $document->setValue('r_time', $request->get('r_time'));
        $document->setValue('remark_doctor', $request->get('remark_doctor'));
        $document->setValue('doctor_name', $request->get('doctor_name'));
        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
