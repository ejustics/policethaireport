<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report11;
use App\Report12;
use App\Report13;
use App\Report14;
use App\Report15;
use App\Report16;
use App\Report17;
use App\Report19;
use App\Report20;
use App\Report21;
use App\Report23;
use App\Report25;


class ReportController extends Controller
{
    //
    public function index($id){
        return view('report'.$id,compact('id'));
    }

    public function export(Request $request){

        $reportId = $request->get('reportId');
        if($reportId == 11){
            $report11 = new Report11();
            return $report11->genWordRespons($request);
        }else if($reportId == 12){
            $report12 = new Report12();
            return $report12->genWordRespons($request);
        }else if($reportId == 13){
            $Report13 = new Report13();
            return $Report13->genWordRespons($request);
        }else if($reportId == 14){
            $Report14 = new Report14();
            return $Report14->genWordRespons($request);
        }else if($reportId == 15){
            $Report15 = new Report15();
            return $Report15->genWordRespons($request);
        }else if($reportId == 16){
            $Report16 = new Report16();
            return $Report16->genWordRespons($request);
        }else if($reportId == 17){
            $Report17 = new Report17();
            return $Report17->genWordRespons($request);
        }else if($reportId == 19){
            $Report19 = new Report19();
            return $Report19->genWordRespons($request);
        }else if($reportId == 20){
            $Report20 = new Report20();
            return $Report20->genWordRespons($request);
        }else if($reportId == 21){
            $Report21 = new Report21();
            return $Report21->genWordRespons($request);
        }else if($reportId == 23){
            $Report23 = new Report23();
            return $Report23->genWordRespons($request);
        }else if($reportId == 25){
            $Report25 = new Report25();
            return $Report25->genWordRespons($request);
        }
    }
}
