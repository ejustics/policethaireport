<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Report15 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report15.docx";
        $file_name = "บัญชีทรัพย์ที่ถูกเพลิงไหม้.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('station', $request->get('station'));
        $document->setValue('cNo1', $request->get('cNo1'));
        $document->setValue('cNo2', $request->get('cNo2'));
        $document->setValue('regis_date', $request->get('regis_date'));
        $document->setValue('accuser', $request->get('accuser'));
        $document->setValue('accused', $request->get('accused'));
        $document->setValue('accusation', $request->get('accusation'));

        //loop row
        $count = count($request->get('no'));
        $row = 19;
        if($count < $row)
            $document->cloneRow('no', $row );
        else
        {
            $document->cloneRow('no', $count);
            $row = $count;
        }

        $no = $request->input('no');
        $desc = $request->input('desc');
        $amount = $request->input('n');
        $price = $request->input('price');
        $from = $request->input('from');
        $date = $request->input('date');
        $remark = $request->input('remark');

        for ($i = 0; $i < $row; $i++){
            $idx = ($i+1);
            if($i < $count){
                $document->setValue('no#'.$idx, $no[$i]);
                $document->setValue('desc#'.$idx, $desc[$i]);
                $document->setValue('n#'.$idx, $amount[$i]);
                $document->setValue('price#'.$idx, $price[$i]);
                $document->setValue('from#'.$idx, $from[$i]);
                $document->setValue('date#'.$idx, $date[$i]);
                $document->setValue('remark#'.$idx, $remark[$i]);
            }else{
                $document->setValue('no#'.$idx, '');
                $document->setValue('desc#'.$idx, '');
                $document->setValue('n#'.$idx, '');
                $document->setValue('price#'.$idx, '');
                $document->setValue('from#'.$idx, '');
                $document->setValue('date#'.$idx, '');
                $document->setValue('remark#'.$idx, '');
            }
        }

        $document->saveAs($path.$file_name);

        return response()->download($path.$file_name);
    }
}
