<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class report23 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report23.docx";
        $file_name = "รายงานการชันสูตรพลิกศพ.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('no', $request->get('no'));
        $document->setValue('station', $request->get('station'));
        $document->setValue('s_city', $request->get('s_city'));
        $document->setValue('s_province', $request->get('s_province'));

        $document->setValue('day', $request->get('day'));
        $document->setValue('month', $request->get('month'));
        $document->setValue('year', $request->get('year'));

        $document->setValue('dead_name', $request->get('dead_name'));
        $document->setValue('dead_age', $request->get('dead_age'));
        $document->setValue('dead_sex', $request->get('dead_sex'));
        $document->setValue('dead_race', $request->get('dead_race'));
        $document->setValue('dead_nationality', $request->get('dead_nationality'));
        $document->setValue('dead_religion', $request->get('dead_religion'));
        $document->setValue('dead_father', $request->get('dead_father'));
        $document->setValue('dead_mather', $request->get('dead_mather'));

        $document->setValue('address', $request->get('address'));
        $document->setValue('n', $request->get('n'));
        $document->setValue('ditrict', $request->get('ditrict'));
        $document->setValue('city', $request->get('city'));
        $document->setValue('province', $request->get('province'));

        $document->setValue('dead_date', $request->get('dead_date'));
        $document->setValue('find_date', $request->get('find_date'));
        $document->setValue('r_time', $request->get('r_time'));
        $document->setValue('remark_doctor', $request->get('remark_doctor'));
        $document->setValue('doctor_name', $request->get('doctor_name'));

        $document->setValue('d_address', $request->get('d_address'));
        $document->setValue('d_ditrict', $request->get('d_ditrict'));
        $document->setValue('d_city', $request->get('d_city'));
        $document->setValue('d_province', $request->get('d_province'));

        $document->setValue('f_address', $request->get('f_address'));
        $document->setValue('f_ditrict', $request->get('f_ditrict'));
        $document->setValue('f_city', $request->get('f_city'));
        $document->setValue('f_province', $request->get('f_province'));

        $document->setValue('assassin', $request->get('assassin'));
        $document->setValue('founder', $request->get('founder'));
        $document->setValue('staff_dead', $request->get('staff_dead'));
        $document->setValue('root_cause', $request->get('root_cause'));
        $document->setValue('deal_body', $request->get('deal_body'));

        //2
        $document->setValue('remark', $request->get('remark'));
        $document->setValue('s_root_cause', $request->get('s_root_cause'));

        $document->setValue('rm_day', $request->get('rm_day'));
        $document->setValue('rm_month', $request->get('rm_month'));
        $document->setValue('rm_year', $request->get('rm_year'));

        $document->saveAs($path.$file_name);
        return response()->download($path.$file_name);
    }
}
