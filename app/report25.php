<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class report25 extends Model
{
    public function genWordRespons(Request $request){

        $template_name = "report25.docx";
        $file_name = "บันทึกส่งของกลางมาตรวจพิสูจน์.docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $path = base_path('resources/files/');
        $document = $phpWord->loadTemplate($path.$template_name);
        // Variables on different parts of document
        $document->setValue('service', $request->get('service'));
        $document->setValue('tell', $request->get('tell'));
        $document->setValue('no', $request->get('no'));
        $document->setValue('subject', $request->get('subject'));
        $document->setValue('to', $request->get('to'));

        $document->setValue('case_no', $request->get('case_no'));
        $document->setValue('accused', $request->get('accused'));
        $document->setValue('age', $request->get('age'));
        $document->setValue('nationality', $request->get('nationality'));
        $document->setValue('case_date', $request->get('case_date'));
        $document->setValue('case_loc', $request->get('case_loc'));

        $document->setValue('accuser', $request->get('accuser'));
        $document->setValue('age2', $request->get('age2'));
        $document->setValue('nationality2', $request->get('nationality2'));
        $document->setValue('accusation', $request->get('accusation'));

        $document->setValue('case_detail', $request->get('case_detail'));
        $document->setValue('item_loc', $request->get('item_loc'));

        $document->setValue('item1', $request->get('item1'));
        $document->setValue('item2', $request->get('item2'));
        $document->setValue('item3', $request->get('item3'));

        $document->setValue('p_item1', $request->get('p_item1'));
        $document->setValue('p_item2', $request->get('p_item2'));
        $document->setValue('p_item3', $request->get('p_item3'));

        $document->setValue('inquiry_officer', $request->get('inquiry_officer'));
        $document->setValue('staff_name', $request->get('staff_name'));
        $document->setValue('staff_position', $request->get('staff_position'));

        $document->saveAs($path.$file_name);
        return response()->download($path.$file_name);
    }
}
